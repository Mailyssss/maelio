function sortArticles(event) {
    const sortBy = event.target.value;
    const articlesContainer = document.getElementById('articleslist');
    const articles = Array.from(articlesContainer.querySelectorAll('.article'));
  
    articles.sort((a, b) => {
      const aSortValue = a.querySelector(`.article-info .${sortBy}`).getAttribute(`data-${sortBy}`);
      const bSortValue = b.querySelector(`.article-info .${sortBy}`).getAttribute(`data-${sortBy}`);
      
      if (sortBy === 'date') {
        return new Date(bSortValue) - new Date(aSortValue);
      } else {
        return parseInt(bSortValue) - parseInt(aSortValue);
      }
    });
  
    // Ajout de cette ligne pour ajuster la largeur des articles en fonction de la taille de l'écran
    const articlesPerRow = window.innerWidth <= 768 ? 1 : 2;
    articlesContainer.innerHTML = ''; // Vide le conteneur d'articles pour réorganiser les articles
  
    let row = document.createElement('div');
    row.classList.add('article-container');
    articles.forEach((article, index) => {
      if (index % articlesPerRow === 0 && index !== 0) {
        articlesContainer.appendChild(row);
        row = document.createElement('div');
        row.classList.add('article-container');
      }
      row.appendChild(article);
    });
    articlesContainer.appendChild(row);
}
